<?php


namespace app\controllers;


use app\core\models\entities\News;
use app\core\models\search\NewsSearch;
use yii\web\Controller;
use yii\web\HttpException;

class NewsController extends Controller
{

    public function actionIndex(): string
    {
        $search = new NewsSearch();
        $dataProvider = $search->search();
        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    public function actionView(int $id): string
    {
        if ($model = News::findOne($id)) {
            return $this->render('view', ['model' => $model]);
        }
        throw new HttpException(404);
    }

}