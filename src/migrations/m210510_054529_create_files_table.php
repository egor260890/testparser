<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%files}}`.
 */
class m210510_054529_create_files_table extends Migration
{
    private $table = '{{%files}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable($this->table, [
            'id'         => $this->primaryKey(),
            'name'       => $this->string(),
            'extension'  => $this->string(4),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%files}}');
    }
}
