<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%news}}`.
 */
class m210508_142454_create_news_table extends Migration
{
    private $table = '{{%news}}';
    private $authorsTable = '{{%authors}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable($this->table, [
            'id'         => $this->primaryKey(),
            'author_id'  => $this->integer(),
            'title'      => $this->string(),
            'sub_title'  => $this->text(),
            'content'    => $this->text(),
        ], $tableOptions);
        $this->addForeignKey('fk-news-author_id', $this->table, 'author_id', $this->authorsTable, 'id', 'SET NULL', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
