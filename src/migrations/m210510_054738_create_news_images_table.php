<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%news_images}}`.
 */
class m210510_054738_create_news_images_table extends Migration
{

    private $table = '{{%news_images}}';
    private $newsTable = '{{%news}}';
    private $filesTable = '{{%files}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->table, [
            'news_id' => $this->integer(),
            'file_id' => $this->integer(),
        ]);
        $this->addPrimaryKey('fk-news_images', $this->table, ['news_id', 'file_id']);
        $this->addForeignKey('fk-news_images-news_id', $this->table, 'news_id', $this->newsTable, 'id',
            'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-news_images-file_id', $this->table, 'file_id', $this->filesTable, 'id',
            'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }

}
