<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%news_tag_relations}}`.
 */
class m210508_151254_create_news_tag_relations_table extends Migration
{
    private $table = '{{%news_tag_relations}}';
    private $newsTable = '{{%news}}';
    private $tagsTable = '{{%tags}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable($this->table, [
            'news_id' => $this->integer(),
            'tag_id'  => $this->integer(),
        ], $tableOptions);
        $this->addPrimaryKey('fk-news_tag_relations', $this->table, ['news_id', 'tag_id']);
        $this->addForeignKey('fk-news_tag_relations-news_id', $this->table, 'news_id', $this->newsTable, 'id',
            'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-news_tag_relations-tag_id', $this->table, 'tag_id', $this->tagsTable, 'id',
            'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
