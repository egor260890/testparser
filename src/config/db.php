<?php

$dbHost = getenv('DB_HOST');
$dbName = getenv('DB_NAME');
$dbUsername = getenv('DB_USERNAME');
$dbPassword = getenv('DB_PASSWORD');

return [
    'class'    => 'yii\db\Connection',
    'dsn'      => sprintf('mysql:host=%s;dbname=%s', $dbHost, $dbName),
    'username' => $dbUsername,
    'password' => $dbPassword,
    'charset'  => 'utf8',
];
