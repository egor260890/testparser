<?php

use app\core\helpers\StringHelper;
use yii\helpers\Url;

/* @var $dataProvider \yii\data\ActiveDataProvider */
?>

<div class="container">
    <div class="row">
        <?php foreach ($dataProvider->getModels() as $model): ?>
            <div>
                <h1><a href="<?= Url::to(['/news/view', 'id' => $model->id]) ?>"><?= $model->title ?></a></h1>
                <p><?= StringHelper::cut($model->content) ?></p>
                <p><a class="btn btn-success btn-xs" href="<?= Url::to(['/news/view', 'id' => $model->id]) ?>">detail</a></p>
                <?php foreach ($model->tags as $tag): ?>
                    <div class="btn btn-default">
                        <?= $tag->name ?>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>

