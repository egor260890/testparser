<?php


/* @var $model \app\core\models\entities\News */
?>

<div>
    <h1><?= $model->title ?></h1>
    <h3><?= $model->sub_title ?></h3>
    <?php if ($imageFile = $model->imageFile): ?>
        <img src="<?= $imageFile->fileUrl ?>" alt="">
    <?php endif; ?>
    <p><?= $model->content ?></p>
    <?php foreach ($model->tags as $tag): ?>
        <div class="btn btn-default">
            <?= $tag->name ?>
        </div>
    <?php endforeach; ?>
    <h3><?= $model->author->name ?? null ?></h3>
</div>
