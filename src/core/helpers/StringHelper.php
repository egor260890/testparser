<?php


namespace app\core\helpers;


class StringHelper
{

    public static function cut(string $str, int $length = 200, $postfix = '...'): string
    {
        if (strlen($str) <= $length) {
            return $str;
        }
        $temp = substr($str, 0, $length);
        return substr($temp, 0, strrpos($temp, ' ')) . $postfix;
    }


}