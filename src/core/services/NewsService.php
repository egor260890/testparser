<?php


namespace app\core\services;


use app\core\models\entities\Author;
use app\core\models\entities\File;
use app\core\models\entities\FileData;
use app\core\models\entities\News;
use app\core\models\entities\NewsData;
use app\core\models\entities\NewsImage;
use app\core\models\entities\NewsTagRelation;
use app\core\models\entities\Tag;
use Exception;
use Yii;

class NewsService
{

    public function createByData(NewsData $data): ?News
    {
        return $this->transaction(function () use ($data) {
            $author = null;
            if ($data->getAuthor()) {
                $author = $this->getOrCreateAuthor($data->getAuthor());
            }

            $news = News::create($author?->id, $data->getTitle(), $data->getSubtitle(), $data->getContent());
            if (!$news->save()) {
                throw new Exception('news cannot be saved');
            }

            foreach ($data->getTags() as $tagName) {
                $tag = $this->getOrCreateTag($tagName);
                $tagRelation = NewsTagRelation::create($news->id, $tag->id);
                if (!$tagRelation->save()) {
                    throw new Exception('tag relation cannot be saved');
                }
            }

            if ($data->getImage()) {
                $this->attachImage($news->id, $data->getImage());
            }
            return $news;
        });
    }

    private function attachImage(int $newsId, FileData $image): void
    {
        $fileName = time() . Yii::$app->security->generateRandomString(6);
        $file = File::create($fileName, $image->getExtension());
        if (!$file->save()) {
            throw new Exception('file cannot be saved');
        }
        file_put_contents($file->getFilePath(), $image->getContent());
        $newsImage = NewsImage::create($newsId, $file->id);
        if (!$newsImage->save()) {
            throw new Exception('news image cannot be saved');
        }

    }

    private function getOrCreateAuthor(string $name): Author
    {
        if (!$author = Author::findOne(['name' => $name])) {
            $author = Author::create($name);
            if (!$author->save()) {
                throw new Exception('author cannot be saved');
            }
        }
        return $author;
    }

    private function getOrCreateTag(string $name): Tag
    {
        if (!$tag = Tag::findOne(['name' => $name])) {
            $tag = Tag::create($name);
            if (!$tag->save()) {
                throw new Exception('tag cannot be saved');
            }
        }
        return $tag;
    }

    private function transaction(callable $function): mixed
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $result = $function();
            if ($transaction->getIsActive()) {
                $transaction->commit();
                return $result;
            }
        } catch (Exception $exception) {
            $transaction->rollBack();
            throw $exception;
        }
    }

}