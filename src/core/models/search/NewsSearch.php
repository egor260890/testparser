<?php


namespace app\core\models\search;


use app\core\models\entities\News;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

class NewsSearch extends Model
{

    public function search(): ActiveDataProvider
    {
        $query = $this->getQuery();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);
        return $dataProvider;
    }

    protected function getQuery(): ActiveQuery
    {
        return News::find()
            ->alias('n')
            ->joinWith(['tags t', 'author a', 'imageFile i'])
            ->groupBy(['n.id']);
    }

}