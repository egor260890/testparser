<?php


namespace app\core\models\entities;


interface FileData
{

    public function getContent(): string;

    public function getExtension(): string;

}