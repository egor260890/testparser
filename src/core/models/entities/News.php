<?php


namespace app\core\models\entities;


use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class News
 * @package app\core\models\entities
 *
 * @property int $id
 * @property int $author_id
 * @property string $title
 * @property string $sub_title
 * @property string $content
 */
class News extends ActiveRecord
{
    public static function create(
        ?int $authorId,
        string $title,
        ?string $subTitle,
        string $content
    ): static
    {
        $model = new static();
        $model->author_id = $authorId;
        $model->title = $title;
        $model->sub_title = $subTitle;
        $model->content = $content;
        return $model;
    }

    public function getAuthor(): ActiveQuery
    {
        return $this->hasOne(Author::class, ['id' => 'author_id']);
    }

    public function getTagRelations(): ActiveQuery
    {
        return $this->hasMany(NewsTagRelation::class, ['news_id' => 'id']);
    }

    public function getTags(): ActiveQuery
    {
        return $this->hasMany(Tag::class, ['id' => 'tag_id'])->via('tagRelations');
    }

    public function getNewsImage(): ActiveQuery
    {
        return $this->hasOne(NewsImage::class, ['news_id' => 'id']);
    }

    public function getImageFile(): ActiveQuery
    {
        return $this->hasOne(File::class, ['id' => 'file_id'])->via('newsImage');
    }

    public static function tableName(): string
    {
        return '{{%news}}';
    }

}