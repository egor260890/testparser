<?php


namespace app\core\models\entities;


use yii\db\ActiveRecord;

/**
 * Class NewsImage
 * @package app\core\models\entities
 *
 * @property int $news_id
 * @property int $file_id
 */
class NewsImage extends ActiveRecord
{

    public static function create(int $newsId, int $fileId): static
    {
        $model = new static();
        $model->news_id = $newsId;
        $model->file_id = $fileId;
        return $model;
    }

    public static function tableName(): string
    {
        return '{{%news_images}}';
    }

}