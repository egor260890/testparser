<?php

namespace app\core\models\entities;

use yii\db\ActiveRecord;

/**
 * Class Author
 * @package app\core\models\entities
 *
 * @property int $id
 * @property int $name
 */
class Author extends ActiveRecord
{


    public static function create(string $name): static
    {
        $model = new static();
        $model->name = $name;
        return $model;
    }

    public static function tableName(): string
    {
        return '{{%authors}}';
    }

}