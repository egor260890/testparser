<?php


namespace app\core\models\entities;


use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Url;

/**
 * Class File
 * @package app\core\models\entities
 *
 * @property int $id
 * @property string $name
 * @property string $extension
 */
class File extends ActiveRecord
{

    public static function create(string $name, string $extension): static
    {
        $model = new static();
        $model->name = $name;
        $model->extension = $extension;
        return $model;
    }

    public function getFilePath(): string
    {
        return Yii::getAlias('@uploads/') . $this->name . '.' . $this->extension;
    }

    public function getFileUrl(): string
    {
        return Url::to(['/uploads/' . $this->name . '.' . $this->extension]);
    }

    public static function tableName(): string
    {
        return '{{%files}}';
    }

}