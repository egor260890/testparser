<?php


namespace app\core\models\entities;


interface NewsData
{

    public function getTitle(): string;

    public function getSubtitle(): ?string;

    public function getContent(): ?string;

    public function getAuthor(): ?string;

    public function getTags(): array;

    public function getImage(): ?FileData;

}