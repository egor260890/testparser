<?php


namespace app\core\models\entities;


use yii\db\ActiveRecord;

/**
 * Class NewsTagRelation
 * @package app\core\models\entities
 *
 * @property int $news_id
 * @property int $tag_id
 */
class NewsTagRelation extends ActiveRecord
{
    public static function create(int $newsId, int $tagId): static
    {
        $model = new static();
        $model->news_id = $newsId;
        $model->tag_id = $tagId;
        return $model;
    }

    public static function tableName(): string
    {
        return '{{%news_tag_relations}}';
    }

}