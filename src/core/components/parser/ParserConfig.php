<?php


namespace app\core\components\parser;


class ParserConfig
{

    public function __construct(private int $limit = 100) { }

    public function getLimit(): int
    {
        return $this->limit;
    }

}