<?php


namespace app\core\components\parser\data;


class FileData implements \app\core\models\entities\FileData
{

    public function __construct(private string $content, private string $extension){}

    public function getContent(): string
    {
        return  $this->content;
    }

    public function getExtension(): string
    {
        return  $this->extension;
    }
}