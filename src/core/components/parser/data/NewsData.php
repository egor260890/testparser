<?php

namespace app\core\components\parser\data;


use app\core\models\entities\FileData;
use app\core\models\entities\NewsData as NewsDataInterface;

class NewsData implements NewsDataInterface
{

    public function __construct(
        private string $title,
        private ?string $subtitle,
        private string $content,
        private ?string $author,
        private array $tags,
        private ?FileData $image
    )
    {
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getSubtitle(): ?string
    {
        return $this->subtitle;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function getTags(): array
    {
        return $this->tags;
    }

    public function getImage(): ?FileData
    {
        return $this->image;
    }
}