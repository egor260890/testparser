<?php

namespace app\core\components\parser;

use app\core\components\parser\data\FileData;
use app\core\components\parser\data\NewsData;
use Generator;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use HTML_Node;
use Pharse;

class RbcParser extends ParserBase
{

    const BASE_URL = 'https://www.rbc.ru/';

    private Client $client;
    private CookieJar $cookie;

    public function __construct()
    {
        $guzzleConfig = [
            'allow_redirects' => true,
        ];
        $this->client = new Client($guzzleConfig);
        $this->cookie = new CookieJar();
        parent::__construct();
    }

    protected static function getId(): string
    {
        return 'rbc';
    }

    public function run(): Generator
    {
        $dom = $this->request(self::BASE_URL);
        $news = $dom->select('.js-news-feed-list', 0)->select('.news-feed__item');
        $links = array_map(fn($item) => $item->getAttribute('href'), $news);
        $counter = 0;
        foreach ($links as $link) {
            if ($counter >= $this->parserConfig->getLimit()) {
                break;
            }
            $this->delay();
            if ($data = $this->parseArticle($link)) {
                $counter++;
                yield $data;
            }
        }
    }

    private function parseArticle(string $uri): ?NewsData
    {
        $dom = $this->request($uri);
        $article = $dom->select('.l-col-main.article__content', 0);
        if (!$article) {
            return null;
        }
        $title = $article->select('.article__header__title-in', 0)?->getPlainText();
        $subTitle = $article->select('.article__text__overview', 0)?->getPlainText();
        $contentBlocks = $article->select('.article__text', 0)->select('p');
        $contents =
            implode(' ', array_filter(array_map(fn($content) => trim($content?->getPlainText()), $contentBlocks)));
        $imageLink = $article->select('.article__main-image__wrap', 0)?->select('img', 0)?->getAttribute('src');
        $author = $article->select('.article__authors__author__name', 0)?->getPlainText();
        $tagBlocks = $article->select('.article__tags__item');
        $tags = $content = array_filter(array_map(fn($tag) => trim($tag?->getPlainText()), $tagBlocks));
        $image = null;
        if ($imageLink) {
            $info = pathinfo($imageLink);
            $imageContent = file_get_contents($imageLink);
            $image = new FileData($imageContent, $info['extension']);
        }
//        var_dump($title);
//        var_dump($subTitle);
//        var_dump($contents);
//        var_dump($imageLink);
//        var_dump($author);
//        var_dump($tags);
        return new NewsData($title, $subTitle, $contents, $author, $tags, $image);
    }

    private function request(string $uri): HTML_Node
    {
        var_dump($uri);
        $response = $this->client->request('GET', $uri,
            ['cookies' => $this->cookie, 'headers' => $this->getRequestHeaders()]);
        $content = $response->getBody()->getContents();
        return Pharse::str_get_dom($content);
    }

    private function delay(): void
    {
        sleep(rand(2, 5));
    }

    private function getRequestHeaders(): array
    {
        return [
            'User-Agent'      => 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:84.0) Gecko/20100101 Firefox/84.0',
            'Cache-control'   => 'max-age=0',
            'Accept-Encoding' => 'gzip, deflate',
            'Accept-Language' => 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
            'Accept'          => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
//            'Host'            => '62.106.26.218:8000',
        ];
    }
}