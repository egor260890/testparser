<?php

namespace app\core\components\parser;

use Generator;

abstract class ParserBase
{

    public function __construct(protected ?ParserConfig $parserConfig = null)
    {
        if (!$this->parserConfig) {
            $this->parserConfig = new ParserConfig();
        }
    }

    public function setConfig(ParserConfig $parserConfig): static
    {
        $this->parserConfig = $parserConfig;
        return $this;
    }

    public static function matchId(string $id): bool
    {
        return static::getId() === $id;
    }

    protected abstract static function getId(): string;

    public abstract function run(): Generator;

}