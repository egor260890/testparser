<?php

namespace app\core\components\parser;

use Exception;

class ParserFactory
{

    public array $parserClients = [
        RbcParser::class
    ];

    public function createById(string $id): ParserBase
    {
        foreach ($this->parserClients as $client) {
            if (is_subclass_of($client, ParserBase::class) && forward_static_call([RbcParser::class, 'matchId'], $id)) {
                return new $client();
            }
        }
        throw new Exception('Invalid id parameter');
    }

}