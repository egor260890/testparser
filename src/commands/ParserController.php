<?php


namespace app\commands;


use app\core\components\parser\ParserConfig;
use app\core\components\parser\ParserFactory;
use app\core\services\NewsService;
use Exception;
use yii\console\Controller;

class ParserController extends Controller
{

    public function __construct(
        $id,
        $module,
        private ParserFactory $parserFactory,
        private NewsService $newsService,
        $config = []
    )
    {
        parent::__construct($id, $module, $config);
    }

    public function actionIndex(string $clientId): void
    {
        $parserConfig = new ParserConfig(10);
        $newsGenerator = $this->parserFactory->createById($clientId)->setConfig($parserConfig)->run();
        foreach ($newsGenerator as $item) {
            try {
                $this->newsService->createByData($item);
            } catch (Exception $e) {
                var_dump($e->getTraceAsString());
                //handle exception
            }
        }
    }

}