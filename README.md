INSTALLATION
------------

Run the following commands:

~~~
docker-compose up -d --build
~~~

~~~
docker exec -t -w /var/www/html/src testparser_php composer install
~~~

~~~
docker exec -t testparser_php php src/yii migrate --interactive=0
~~~

~~~
docker exec -t testparser_php php src/yii parser rbc
~~~

Or execute bash script:

~~~
./install.sh
~~~

See:

~~~
http://localhost:8080
~~~